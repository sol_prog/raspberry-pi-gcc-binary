# GCC 10.1 binary for Raspberry Pi #

I've compiled *GCC 10.1.0* for Raspberry Pi, enabled languages C, C++ and Fortran. The compilers should work with all current versions of Pi.
You can read more about how I've cross compiled *GCC* at [https://solarianprogrammer.com/2018/05/06/building-gcc-cross-compiler-raspberry-pi/](https://solarianprogrammer.com/2018/05/06/building-gcc-cross-compiler-raspberry-pi/) and you can repeat the process yourself if you wish to recreate the binary on your Pi.

How to use:
===========

You can find a detailed tutorial on how to install the above binary at [https://solarianprogrammer.com/2017/12/08/raspberry-pi-raspbian-install-gcc-compile-cpp-17-programs/](https://solarianprogrammer.com/2017/12/08/raspberry-pi-raspbian-install-gcc-compile-cpp-17-programs/).
